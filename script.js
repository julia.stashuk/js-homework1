//Теоретичні питання
//1. Ми можемо оголосити змінні за допомогою ключових слів let чи const (значення змінної не може змінюватися), а також через застаріле ключове слово var.
//2. Відмінність полягає в тому, що prompt показує повідомлення з проханням ввести текст, повертає цей текст або null, якщо натиснута кнопка “Скасувати” або клавіша Esc, а confirm показує повідомлення і чекає, коли користувач натисне “OK” або “Скасувати” і повертає true для ОК та false для “Скасувати”/Esc.
//3. Неявне перетворення типів - це автоматичне перетворення типів при компіляції. Наприклад, рядка в число.
// let num1 = 2; number
// let num2 = "3"; string
// let sum = num1 + num2; number + string
//console.log(sum) покаже число 5

//Завдання 1
let admin;
let userName;
userName="Yuliia";
admin = userName;
console.log(admin);

//Завдання 2
let days = 7;
let seconds = days *24*60*60;
console.log(seconds);

//Завдання 3
let language = prompt("What language do you speak?");
alert("You speak " + language);
console.log(language);